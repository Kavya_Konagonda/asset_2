import { LightningElement ,track} from 'lwc';

export default class StopWatch extends LightningElement {
    @track showStart = true;
    @track timeVal= '0:0:0:0';
    timeIntervalInstance;
    totalMilliseconds = 0;
    @track currentTime;
    
    connectedCallback(){
        this.getCurrentTime(); 
       setInterval( () => {
           this.getCurrentTime();
       }, 1000*60)
    }

    getCurrentTime(){
        const date = new Date();
        const hour = date.getHours();
        const min  = date.getMinutes();
        this.currentTime = (hour > 12 ? hour-12 : hour) + ":" + min + " " +(hour > 12 ? 'PM' : 'AM' )
        
    }
  
    handleStart(event) {

        let parentThis = this;
        parentThis.showStart=false;
        this.timeIntervalInstance = setInterval(function() {
           
          
            let hours = Math.floor( (parentThis.totalMilliseconds/(1000*60*60)) % 24 );
            let minutes = Math.floor((parentThis.totalMilliseconds/1000/60) % 60 );
            let seconds= Math.floor( (parentThis.totalMilliseconds/1000) % 60 );
            let milliseconds = Math.floor((parentThis.totalMilliseconds % (1000)));
            parentThis.timeVal = hours + ":" + minutes + ":" + seconds + ":" + milliseconds;   
            parentThis.totalMilliseconds += 100;
        }, 100);
    }

    handleStop(event) {

        this.showStart=true;
        clearInterval(this.timeIntervalInstance);
    }

    handleReset(event) {
        
        this.showStart=true;
        this.timeVal = '0:0:0:0';
        this.totalMilliseconds = 0;
        clearInterval(this.timeIntervalInstance);
    }

}